﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;

// Source: https://github.com/micahosborne/uFrame/blob/master/Plugins/uFrame/uFrameKernel/EventAggregator.cs

public class EventId : Attribute
{
    public EventId(int identifier)
    {
        Identifier = identifier;
    }

    public int Identifier { get; set; }

    public EventId()
    {
    }
}

public interface IEventKeeper
{
    int EventId { get; set; }
    Type For { get; }
    void Publish(object evt);
}

public class EventManager<TEventType> : IEventKeeper
{
    private Subject<TEventType> _eventType;

    public Subject<TEventType> EventSubject
    {
        get { return _eventType ?? (_eventType = new Subject<TEventType>()); }
        set { _eventType = value; }
    }

    private int _eventId;
    public int EventId
    {
        get
        {
            if (_eventId > 0)
                return _eventId;

            var eventIdAttribute = For.GetCustomAttributes(typeof(EventId), true).FirstOrDefault() as EventId;
            if (eventIdAttribute != null)
            {
                return _eventId = eventIdAttribute.Identifier;
            }
            return _eventId;
        }
        set { _eventId = value; }
    }

    public Type For { get { return typeof(TEventType); } }
    public void Publish(object evt)
    {
        if (_eventType != null)
        {
            _eventType.OnNext((TEventType)evt);
        }
    }
}

public interface IEventAggregator
{
    IObservable<TEvent> GetEvent<TEvent>();
    void Publish<TEvent>(TEvent evt);
}

public class EcsEventAggregator : IEventAggregator
{
    private Dictionary<Type, IEventKeeper> _managers;

    public Dictionary<Type, IEventKeeper> Managers
    {
        get { return _managers ?? (_managers = new Dictionary<Type, IEventKeeper>()); }
        set { _managers = value; }
    }
    private Dictionary<int, IEventKeeper> _managersById;

    public Dictionary<int, IEventKeeper> ManagersById
    {
        get { return _managersById ?? (_managersById = new Dictionary<int, IEventKeeper>()); }
        set { _managersById = value; }
    }

    public IEventKeeper GetEventManager(int eventId)
    {
        if (ManagersById.ContainsKey(eventId))
            return ManagersById[eventId];
        return null;
    }

    public IObservable<TEvent> GetEvent<TEvent>()
    {
        IEventKeeper eventManager;
        if (!Managers.TryGetValue(typeof(TEvent), out eventManager))
        {
            eventManager = new EventManager<TEvent>();
            Managers.Add(typeof(TEvent), eventManager);
            var eventId = eventManager.EventId;
            if (eventId > 0)
            {
                ManagersById.Add(eventId, eventManager);
            }
            else
            {
                // create warning here that eventid attribute is not set
            }
        }
        var em = eventManager as EventManager<TEvent>;
        if (em == null) return null;
        return em.EventSubject;
    }

    public void Publish<TEvent>(TEvent evt)
    {
        IEventKeeper eventManager;

        if (!Managers.TryGetValue(evt.GetType(), out eventManager))
        {
            // No listeners anyways
            return;
        }
        eventManager.Publish(evt);
    }

    public void PublishById(int eventId, object data)
    {
        var evt = GetEventManager(eventId);
        if (evt != null)
            evt.Publish(data);
    }
}
