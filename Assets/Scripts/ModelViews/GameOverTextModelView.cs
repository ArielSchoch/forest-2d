﻿using UnityEngine;
using UniRx;

public class GameOverTextModelView : ModelView
{
    public bool Visible
    {
        get { return gameObject.activeInHierarchy; }
        set { gameObject.SetActive(value); }
    }
}
