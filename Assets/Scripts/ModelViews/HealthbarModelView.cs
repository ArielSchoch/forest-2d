﻿using UnityEngine;

public class HealthbarModelView : ModelView, IHealthbarModelView
{
    [SerializeField] private RectTransform _healthImage;
    [SerializeField] private float _healthAnimationDuration;
    [SerializeField] private LeanTweenType _tweenAnimation;

	public void SetHealth(float health)
	{
        LeanTween.value(_healthImage.anchorMax.x, health, _healthAnimationDuration)
            .setEase(_tweenAnimation)
            .setOnUpdate(hp => UpdateHealthPercentage(hp));
	}

    private void UpdateHealthPercentage(float percent)
    {
        if (_healthImage == null) return;

        Vector2 newMax = _healthImage.anchorMax;
        newMax.x = percent;
        _healthImage.anchorMax = newMax;
    }
}
