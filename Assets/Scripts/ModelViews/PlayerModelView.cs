﻿using UnityEngine;
using UniRx;

[RequireComponent(typeof(Rigidbody2D), typeof(Animator))]
public class PlayerModelView : ModelView, IPlayerModelView
{
    private readonly ReactiveProperty<float> _healthReactive = new ReactiveProperty<float>(1f);
    public ReactiveProperty<float> HealthReactive { get { return _healthReactive; } }

    [SerializeField] private float _moveSpeed = 3f;
    public float MoveSpeed { get { return _moveSpeed; } }

    [SerializeField] private float _attackDuration = 0.3f;
    public float AttackDuration { get { return _attackDuration; } }

    [SerializeField] private float _attackRange = 1.5f;
    public float AttackRange { get { return _attackRange; } }

    [SerializeField] private float _attackDamage = 0.4f;
    public float AttackDamage { get { return _attackDamage; } }

    [SerializeField] private Color _hitParticlesColor;
    public Color HitParticlesColor { get { return _hitParticlesColor; } }

    [SerializeField] private Vector3 _hitParticlesPositionOffset = new Vector3(0f, 1f, 0f);
    public Vector3 HitParticlesPositionOffset { get { return _hitParticlesPositionOffset; } }

    public Vector3 Position
    {
        get { return transform.position; }
        set { transform.position = value; }
    }

    public Vector3 Velocity
    {
        get { return _rigidbody2D.velocity; }
        set { _rigidbody2D.velocity = value; }
    }

    public bool FlipX
    {
        get { return _spriteRenderer.flipX; }
        set { _spriteRenderer.flipX = value; }
    }

    private bool _isMoving;
    public bool IsMoving
    {
        get { return _isMoving; }
        set
        {
            _isMoving = value;
            _animator.SetBool(WalkingTrigger, value);
        }
    }

    public bool IsAttacking { get; set; }

    public bool IsDead { get; set; }

    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private float _damageFadeDuration = 0.2f;
    [SerializeField] private Color _damageColor = Color.red;

    private Rigidbody2D _rigidbody2D;
    private Animator _animator;
    private Color _initialColor;

    private const string WalkingTrigger = "IsWalking";
    private const string AttackTrigger = "Attack";

    protected override void Initialize()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _initialColor = _spriteRenderer.color;
    }

    public void Attack()
    {
        _animator.SetTrigger(AttackTrigger);
    }

    public void PlayHitAnimation()
    {
        _spriteRenderer.color = _damageColor;
        LeanTween.color(_spriteRenderer.gameObject, _initialColor, _damageFadeDuration);
    }

    public void Die()
    {
        gameObject.SetActive(false);
    }
}
