﻿using UnityEngine;
using UniRx;

public class CameraShakeModelView : ModelView, ICameraShakeModelView
{
    [SerializeField] private Vector3 _deltaPos = new Vector3(0f, 0.1f, 0f);
    [SerializeField] private float _duration = 0.2f;

    public void PlayShakeAnimation()
    {
        LeanTween.move(gameObject, transform.position + _deltaPos, _duration).setEase(LeanTween.shake);
    }
}
