﻿using UnityEngine;
using System;
using System.Collections;
using UniRx;

[RequireComponent(typeof(Rigidbody2D), typeof(Animator))]
public class EnemyModelView : ModelView, IEnemyModelView
{
    private readonly Subject<Unit> _onPlayAttackAnimationHitFrame = new Subject<Unit>();
    public IObservable<Unit> OnPlayAttackAnimationHitFrame { get { return _onPlayAttackAnimationHitFrame; } }

    private readonly ReactiveProperty<Vector3> _enemyPositionReactive = new ReactiveProperty<Vector3>();
    public ReactiveProperty<Vector3> EnemyPositionReactive { get { return _enemyPositionReactive; } }

    private readonly ReactiveProperty<float> _healthReactive = new ReactiveProperty<float>(1f);
    public ReactiveProperty<float> HealthReactive { get { return _healthReactive; } }

    [SerializeField] private float _moveSpeed = 1.5f;
    public float MoveSpeed { get { return _moveSpeed; } set { _moveSpeed = value; } }

    [SerializeField] private float _followDistance = 6f;
    public float FollowDistance { get { return _followDistance; } set { _followDistance = value; } }

    [SerializeField] private float _attackDistance = 1.5f;
    public float AttackDistance { get { return _attackDistance; } set { _attackDistance = value; } }

    [SerializeField] private float _attackDelay = 0.2f;
    public float AttackDelay { get { return _attackDelay; } }

    [SerializeField] private float _attackDuration = 0.8f;
    public float AttackDuration { get { return _attackDuration; } }

    [SerializeField] private float _attackDamage = 0.4f;
    public float AttackDamage { get { return _attackDamage; } }

    [SerializeField] private Color _hitParticlesColor;
    public Color HitParticlesColor { get { return _hitParticlesColor; } }

    [SerializeField] private Vector3 _hitParticlesPositionOffset = new Vector3(0f, 1f, 0f);
    public Vector3 HitParticlesPositionOffset { get { return _hitParticlesPositionOffset; } }

    public bool IsAttacking { get; set; }
    public bool IsDead { get; set; }

    public Vector3 Position
    {
        get { return transform.position; }
        set { transform.position = value; }
    }

    public Vector3 Velocity
    {
        get { return _rigidbody2D.velocity; }
        set { _rigidbody2D.velocity = value; }
    }

    public bool FlipX
    {
        get { return transform.localScale.x < 0f; }
        set
        {
            var localScale = transform.localScale;
            localScale.x = value ? -1f : 1f;
            transform.localScale = localScale;
        }
    }

    private bool _isMoving;
    public bool IsMoving
    {
        get { return _isMoving; }
        set
        {
            _isMoving = value;
            _animator.SetBool(WalkingTrigger, value);
        }
    }

    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private float _damageFadeDuration = 0.2f;
    [SerializeField] private Color _damageColor = Color.red;

    private Rigidbody2D _rigidbody2D;
    private Animator _animator;
    private Color _initialColor;

    private const string WalkingTrigger = "IsWalking";
    private const string AttackTrigger = "Attack";
    private const string DieTrigger = "Die";
    private const string NonInteractableLayer = "NonInteractable";

    protected override void Initialize()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _initialColor = _spriteRenderer.color;
    }

	private void Update()
	{
        if (IsDead) return;

        EnemyPositionReactive.Value = transform.position;
	}

    public void Attack(Action onComplete)
    {
        _animator.SetTrigger(AttackTrigger);
        StartCoroutine(WaitForAttackAnimation(onComplete));
    }

    private IEnumerator WaitForAttackAnimation(Action onComplete)
    {
        var attackAnimationDuration = _animator.GetCurrentAnimatorStateInfo(0).length;
        yield return new WaitForSeconds(attackAnimationDuration);
        onComplete();
    }

    public void PlayHitAnimation()
    {
        _spriteRenderer.color = _damageColor;
        LeanTween.color(_spriteRenderer.gameObject, _initialColor, _damageFadeDuration);
    }

	public void Die()
	{
        gameObject.layer = LayerMask.NameToLayer(NonInteractableLayer);
        _animator.SetTrigger(DieTrigger);
	}

    public void OnAttackAnimationHitFrame()
    {
        _onPlayAttackAnimationHitFrame.OnNext(Unit.Default);
    }
}
