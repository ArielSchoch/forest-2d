﻿using UnityEngine;
using UniRx;

public class LevelFinishTriggerModelView : ModelView, ILevelFinishTriggerModelView
{
    private Subject<Unit> _levelCompleteStream = new Subject<Unit>();
    public IObservable<Unit> LevelCompleteStream { get { return _levelCompleteStream; } }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
            _levelCompleteStream.OnNext(Unit.Default);
    }
}
