using UnityEngine;
using UniRx;

public class PlayerKeyboardInputModelView : ModelView, IPlayerInputModelView
{
    [SerializeField] private KeyCode _attackKey = KeyCode.Space;
    [SerializeField] private string _moveAxis = "Horizontal";

    private Subject<float> _moveInputStream = new Subject<float>();
    public IObservable<float> MoveInputStream { get { return _moveInputStream; } }
    private Subject<Unit> _attackInputStream = new Subject<Unit>();
    public IObservable<Unit> AttackInputStream { get { return _attackInputStream; } }

    protected override void Initialize()
    {
        InputBroker.OnKeyDown(_attackKey).Subscribe(_ => _attackInputStream.OnNext(Unit.Default)).AddTo(Disposer);
    }

    private void Update()
    {
        _moveInputStream.OnNext(InputBroker.GetAxis(_moveAxis));
    }
}
