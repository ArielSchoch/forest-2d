﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

public class IsActiveWrapper<T>
{
    public T Instance { get; set; }
    public bool IsActive { get; set; }
}

public class ObjectPool<T> where T : class
{
    public virtual int PoolSize { get { return _instances.Count; } }
    protected List<IsActiveWrapper<T>> _instances = new List<IsActiveWrapper<T>>();
    protected Func<T> _createInstanceDelegate;
    protected Action<T, bool> _activateInstanceDelegate;

    public ObjectPool(Func<T> createInstance, Action<T, bool> activateInstance, int startSize = 5)
    {
        _createInstanceDelegate = createInstance;
        _activateInstanceDelegate = activateInstance;

        for (int i = 0; i < startSize; i++)
            CreateInstance();
    }

    public virtual T GetInstance()
    {
        var instanceWrapper = _instances.FirstOrDefault(item => !item.IsActive);

        T instance = (instanceWrapper == null) ? CreateInstance() : instanceWrapper.Instance;
        SetInstanceActive(instance, true);

        return instance;
    }

    public virtual void ReturnToPool(T instance)
    {
        SetInstanceActive(instance, false);
    }

    protected virtual T AddInstance(T instance, bool isActive = false)
    {
        _instances.Add(new IsActiveWrapper<T> { Instance = instance, IsActive = isActive });
        _activateInstanceDelegate(instance, isActive);
        return instance;
    }

    protected virtual void RemoveInstance(T instance)
    {
        _instances.RemoveAt(_instances.FindIndex(item => item.Instance == instance));
    }

    protected virtual T CreateInstance()
    {
        return AddInstance(_createInstanceDelegate());
    }

    protected virtual void SetInstanceActive(T instance, bool isActive)
    {
        _instances.Single(item => item.Instance == instance).IsActive = isActive;
        _activateInstanceDelegate(instance, isActive);
    }
}
