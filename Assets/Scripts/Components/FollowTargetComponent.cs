﻿using UnityEngine;

public class FollowTargetComponent : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private bool _followX;
    [SerializeField] private bool _followY;
    [SerializeField] private bool _followZ;
    [SerializeField] private Vector3 _offset;

    public Transform Target
    {
        get { return _target; }
        set { _target = value; }
    }

    private void LateUpdate()
    {
        var targetPos = transform.position;

        if (_followX)
            targetPos.x = _target.position.x + _offset.x;

        if (_followY)
            targetPos.y = _target.position.y + _offset.y;

        if (_followZ)
            targetPos.z = _target.position.z + _offset.z;

        transform.position = targetPos;
    }
}
