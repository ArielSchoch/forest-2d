﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class InputBroker : MonoBehaviour, IInputBroker
{
    private Dictionary<KeyCode, Subject<KeyCode>> _onKeyDownObservables = new Dictionary<KeyCode, Subject<KeyCode>>();
    private Dictionary<KeyCode, Subject<KeyCode>> _onKeyUpObservables = new Dictionary<KeyCode, Subject<KeyCode>>();

    public bool IgnoreInput { get; set; }

    private void Update()
    {
        if (IgnoreInput) return;

        InvokeOnKeyDownEvents();
        InvokeOnKeyUpEvents();
    }

    private void InvokeOnKeyDownEvents()
    {
        foreach (var kvp in _onKeyDownObservables)
            if (Input.GetKeyDown(kvp.Key))
                kvp.Value.OnNext(kvp.Key);
    }

    private void InvokeOnKeyUpEvents()
    {
        foreach (var kvp in _onKeyUpObservables)
            if (Input.GetKeyUp(kvp.Key))
                kvp.Value.OnNext(kvp.Key);
    }

    public bool GetKey(KeyCode keyCode)
    {
        return Input.GetKey(keyCode);
    }

    public bool GetKeyDown(KeyCode keyCode)
    {
        return Input.GetKeyDown(keyCode);
    }

    public bool GetKeyUp(KeyCode keyCode)
    {
        return Input.GetKeyUp(keyCode);
    }

    public float GetAxis(string axisName)
    {
        return Input.GetAxis(axisName);
    }

    public float GetAxisRaw(string axisName)
    {
        return Input.GetAxisRaw(axisName);
    }

    public IObservable<KeyCode> OnKeyDown(KeyCode keyCode)
    {
        if (!_onKeyDownObservables.ContainsKey(keyCode))
            _onKeyDownObservables.Add(keyCode, new Subject<KeyCode>());

        return _onKeyDownObservables[keyCode];
    }

    public IObservable<KeyCode> OnKeyUp(KeyCode keyCode)
    {
        if (!_onKeyUpObservables.ContainsKey(keyCode))
            _onKeyUpObservables.Add(keyCode, new Subject<KeyCode>());

        return _onKeyUpObservables[keyCode];
    }
}
