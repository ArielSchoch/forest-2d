﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteParallaxComponent : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private float _parallax;
    private SpriteRenderer _spriteRenderer;
    private MaterialPropertyBlock _propBlock;
    private float _spriteWidth;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _propBlock = new MaterialPropertyBlock();
        _spriteWidth = _spriteRenderer.sprite.bounds.size.x;
    }

    private void Update()
    {
        // Get the current value of the material properties in the renderer.
        _spriteRenderer.GetPropertyBlock(_propBlock);

        // Assign our new value.
        var offset = _target.position.x * _parallax / _spriteWidth;
        _propBlock.SetFloat("_Offset", offset % 1f);

        // Apply the edited values to the renderer.
        _spriteRenderer.SetPropertyBlock(_propBlock);
    }
}
