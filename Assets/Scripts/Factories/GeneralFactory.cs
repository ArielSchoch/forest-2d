﻿using UnityEngine;
using System;

public class GeneralFactory : MonoBehaviour
{
    [SerializeField] private GameObject _hitParticlesPrefab;

    public void InitializeEnemies(IPlayerModelView playerModelView)
    {
        var enemies = GameObject.FindGameObjectsWithTag("Skeleton");
        foreach (var enemy in enemies)
        {
            var enemyModelView = enemy.GetComponent<IEnemyModelView>();
            new EnemyController(enemyModelView, playerModelView);
        }
    }

    public HitParticlesController CreateHitParticlesController(Transform worldTransform)
    {
        var hitParticlesPoolParent = new GameObject("Hit Particles Pool").transform;
        hitParticlesPoolParent.SetParent(worldTransform);

        Func<ParticleSystem> createInstanceDelegate = () => Instantiate(_hitParticlesPrefab, hitParticlesPoolParent).GetComponent<ParticleSystem>();
        Action<ParticleSystem, bool> activateInstanceDelegate = (instance, active) => instance.gameObject.SetActive(active);

        var particlePool = new ObjectPool<ParticleSystem>(createInstanceDelegate, activateInstanceDelegate, 3);
        return new HitParticlesController(particlePool);
    }

    public GameOverTextController CreateGameOverTextController()
    {
        var gameOverText = GameObject.FindObjectOfType<GameOverTextModelView>();
        return new GameOverTextController(gameOverText);
    }

    public LevelFinishTriggerController CreateLevelFinishTriggerController(ILevelSwitchService levelSwitchService)
    {
        var levelFinishTrigger = GameObject.FindObjectOfType<LevelFinishTriggerModelView>();
        return new LevelFinishTriggerController(levelFinishTrigger, levelSwitchService);
    }
}
