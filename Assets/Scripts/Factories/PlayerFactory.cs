﻿using UnityEngine;

public class PlayerFactory : MonoBehaviour
{
    [SerializeField] private PlayerModelView _playerPrefab;
    [SerializeField] private HealthbarModelView _playerHealthbarPrefab;

    public IPlayerModelView InitializePlayer(Transform worldParent, Transform canvasParent, Camera playerCamera)
    {
        var playerModelView = CreatePlayerInstance(worldParent, new Vector2(-4f, 1f));
        CreatePlayerHealthbar(playerModelView, canvasParent);

        var playerInputModelView = GetPlayerInputModelView();
        new PlayerInputController(playerInputModelView);

        var cameraShake = playerCamera.GetComponent<ICameraShakeModelView>();
        CreatePlayerHitCameraShakeController(cameraShake);

        playerCamera.GetComponent<FollowTargetComponent>().Target = playerModelView.transform;
        return playerModelView;
    }

    public PlayerModelView CreatePlayerInstance(Transform playerParent, Vector2 spawnPos)
    {
        var playerModelView = Instantiate(_playerPrefab, spawnPos, Quaternion.identity, playerParent);
        new PlayerController(playerModelView);
        return playerModelView;
    }

    public HealthbarModelView CreatePlayerHealthbar(PlayerModelView playerModelView, Transform playerHealthbarParent)
    {
        var modelView = Instantiate(_playerHealthbarPrefab, playerHealthbarParent);
        new PlayerHealthbarController(modelView, playerModelView);
        return modelView;
    }

    public void CreatePlayerHitCameraShakeController(ICameraShakeModelView cameraShake)
    {
        new PlayerHitCameraShakeController(cameraShake);
    }

    public IPlayerInputModelView GetPlayerInputModelView()
    {
        return GameObject.FindObjectOfType<PlayerKeyboardInputModelView>();
    }
}
