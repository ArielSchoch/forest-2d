﻿using UniRx;

public interface IPlayerInputModelView
{
    IObservable<float> MoveInputStream { get; }
    IObservable<Unit> AttackInputStream { get; }
}
