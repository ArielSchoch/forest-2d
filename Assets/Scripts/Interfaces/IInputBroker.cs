﻿using UnityEngine;
using UniRx;

public interface IInputBroker
{
    bool IgnoreInput { get; set; }

    bool GetKey(KeyCode keyCode);
    bool GetKeyDown(KeyCode keyCode);
    bool GetKeyUp(KeyCode keyCode);

    float GetAxis(string axisName);
    float GetAxisRaw(string axisName);

    IObservable<KeyCode> OnKeyDown(KeyCode keyCode);
    IObservable<KeyCode> OnKeyUp(KeyCode keyCode);
}
