﻿using UniRx;

public interface IEventSubscriber
{
    IEventAggregator EventStream { get; }
    CompositeDisposable Disposer { get; }
}
