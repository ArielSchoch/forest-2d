﻿using UniRx;

public interface ILevelFinishTriggerModelView
{
    IObservable<Unit> LevelCompleteStream { get; }
}
