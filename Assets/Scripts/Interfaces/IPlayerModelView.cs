using UnityEngine;
using UniRx;

public interface IPlayerModelView
{
    ReactiveProperty<float> HealthReactive { get; }
    float MoveSpeed { get; }
    float AttackDuration { get; }
    float AttackRange { get; }
    float AttackDamage { get; }
    Color HitParticlesColor { get; }
    Vector3 HitParticlesPositionOffset { get; }
    Vector3 Position { get; set; }
    Vector3 Velocity { get; set; }
    bool FlipX { get; set; }
    bool IsMoving { get; set; }
    bool IsAttacking { get; set; }
    bool IsDead { get; set; }

    void Attack();
    void PlayHitAnimation();
    void Die();
}