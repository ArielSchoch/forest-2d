using System;
using UnityEngine;
using UniRx;

public interface IEnemyModelView
{
    IObservable<Unit> OnPlayAttackAnimationHitFrame { get; }
    ReactiveProperty<Vector3> EnemyPositionReactive { get; }
    ReactiveProperty<float> HealthReactive { get; }

    float MoveSpeed { get; set; }
    float FollowDistance { get; set; }
    float AttackDistance { get; set; }
    float AttackDelay { get; }
    float AttackDuration { get; }
    float AttackDamage { get; }
    Color HitParticlesColor { get; }
    Vector3 HitParticlesPositionOffset { get; }
    bool IsAttacking { get; set; }
    bool IsDead { get; set; }
    Vector3 Position { get; set; }
    Vector3 Velocity { get; set; }
    bool FlipX { get; set; }
    bool IsMoving { get; set; }

    void Attack(Action onComplete);
    void PlayHitAnimation();
    void Die();
}
