﻿using System;
using UnityEngine;

public interface ILevelSwitchService
{
    int CurrentLevel { get; }

    void LoadLevel(int level);
    void LoadGameScene(Action<AsyncOperation> onCompleted);
}
