
public class PlayerAttackEvent
{
    public PlayerAttackEvent(float attackRange, float damage)
    {
        AttackRange = attackRange;
        Damage = damage;
    }

    public float AttackRange { get; private set; }
    public float Damage { get; private set; }
}
