﻿
public class LoadSceneEvent
{
    public string SceneName { get; set; }

    public LoadSceneEvent(string sceneName)
    {
        SceneName = sceneName;
    }
}
