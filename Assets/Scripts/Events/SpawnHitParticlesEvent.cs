﻿using UnityEngine;

public class SpawnHitParticlesEvent
{
    public Vector3 Position { get; set; }
    public Color Color { get; set; }
}
