
public class EnemyAttackEvent
{
    public EnemyAttackEvent(float damage)
    {
        Damage = damage;
    }

    public float Damage { get; private set; }
}
