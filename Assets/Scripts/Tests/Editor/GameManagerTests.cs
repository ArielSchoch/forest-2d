﻿using System;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;
using UniRx;

public class InputBrokerStub : IInputBroker
{
    private Dictionary<KeyCode, Subject<KeyCode>> _onKeyDownObservables = new Dictionary<KeyCode, Subject<KeyCode>>();
    private Dictionary<KeyCode, Subject<KeyCode>> _onKeyUpObservables = new Dictionary<KeyCode, Subject<KeyCode>>();
    private CompositeDisposable _disposer = new CompositeDisposable();

    public bool IgnoreInput { get; set; }

    public InputBrokerStub(IObservable<KeyCode> keyDownEvents, IObservable<KeyCode> keyUpEvents)
    {
        keyDownEvents.Where(_ => !IgnoreInput).Subscribe(HandleKeyDownEvent).AddTo(_disposer);
        keyUpEvents.Where(_ => !IgnoreInput).Subscribe(HandleKeyUpEvent).AddTo(_disposer);
    }

    public void Dispose()
    {
        _disposer.Dispose();
    }

    private void HandleKeyDownEvent(KeyCode keyCode)
    {
        if (_onKeyDownObservables.ContainsKey(keyCode))
            _onKeyDownObservables[keyCode].OnNext(keyCode);
    }

    private void HandleKeyUpEvent(KeyCode keyCode)
    {
        if (_onKeyUpObservables.ContainsKey(keyCode))
            _onKeyUpObservables[keyCode].OnNext(keyCode);
    }

    public bool GetKey(KeyCode keyCode)
    {
        return false;
    }

    public bool GetKeyDown(KeyCode keyCode)
    {
        return false;
    }

    public bool GetKeyUp(KeyCode keyCode)
    {
        return false;
    }

    public float GetAxis(string axisName)
    {
        return 0f;
    }

    public float GetAxisRaw(string axisName)
    {
        return 0f;
    }

    public IObservable<KeyCode> OnKeyDown(KeyCode keyCode)
    {
        if (!_onKeyDownObservables.ContainsKey(keyCode))
            _onKeyDownObservables.Add(keyCode, new Subject<KeyCode>());

        return _onKeyDownObservables[keyCode];
    }

    public IObservable<KeyCode> OnKeyUp(KeyCode keyCode)
    {
        if (!_onKeyUpObservables.ContainsKey(keyCode))
            _onKeyUpObservables.Add(keyCode, new Subject<KeyCode>());

        return _onKeyUpObservables[keyCode];
    }
}

public class LevelSwitchServiceSpy : ILevelSwitchService
{
    public int CurrentLevel { get; set; }
    public int LoadLevelCallCount { get; set; }

    public void LoadLevel(int level)
    {
        LoadLevelCallCount++;
    }

    public void LoadGameScene(Action<AsyncOperation> onCompleted)
    {
        throw new NotImplementedException();
    }
}

public class GameManagerTests : TestBehaviour
{
    [Test]
    public void LoadSceneCalledWhenPlayerDiedAndSpaceKeyIsPressed()
    {
        var keyDownEvents = new Subject<KeyCode>();
        var keyUpEvents = new Subject<KeyCode>();

        var inputBroker = new InputBrokerStub(keyDownEvents, keyUpEvents);
        InputBrokerProvider.InputBroker = inputBroker;

        var levelSwitchService = new LevelSwitchServiceSpy();
        new GameManager(levelSwitchService);

        // Publish PlayerDiedEvent
        EventStream.Publish(new PlayerDiedEvent());
        // Fake Space key down event
        keyDownEvents.OnNext(KeyCode.Space);
        // Check that LoadScene() was called
        Assert.AreEqual(1, levelSwitchService.LoadLevelCallCount);
    }
}
