﻿using NUnit.Framework;

public class TestBehaviour
{
    public IEventAggregator EventStream { get; private set; }

    [OneTimeSetUp]
	public virtual void OneTimeSetUp()
    {
        EventStream = new EcsEventAggregator();
        EventAggregatorProvider.EventAggregator = EventStream;
    }
}
