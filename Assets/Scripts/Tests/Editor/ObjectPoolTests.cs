﻿using UnityEngine;
using NUnit.Framework;
using System;

public class ObjectPoolTests
{
    [TestCase(0)]
    [TestCase(1)]
    [TestCase(5)]
    [TestCase(-1)]
    public void InitialPoolSizeMatchesPassedValue(int poolSize)
    {
        var objectPool = new ObjectPool<object>(() => new object(), (instance, active) => {}, poolSize);
        Assert.AreEqual(Math.Max(0, poolSize), objectPool.PoolSize);
    }

    [TestCase(0)]
    [TestCase(1)]
    [TestCase(10)]
    [TestCase(100)]
    public void PoolGrowsWhenNoInstanceAvailable(int poolGrowth)
    {
        var objectPool = new ObjectPool<object>(() => new object(), (instance, active) => {}, 0);

        for (int i = 0; i < poolGrowth; i++)
            Assert.NotNull(objectPool.GetInstance());

        Assert.AreEqual(poolGrowth, objectPool.PoolSize);
    }

    [TestCase(1)]
    [TestCase(10)]
    [TestCase(100)]
    public void CreateInstanceDelegateIsCalled(int poolGrowth)
    {
        int callCount = 0;
        var objectPool = new ObjectPool<object>(() => { callCount++; return new object(); }, (instance, active) => {}, 0);

        objectPool.GetInstance();
        Assert.AreEqual(1, callCount);
    }

    [Test]
    public void ActivateInstanceDelegateIsCalled()
    {
        int callCount = 0;
        var objectPool = new ObjectPool<object>(() => new object(), (instance, active) => callCount++, 1);

        // Deactivate called on all instances during pool intialization
        Assert.AreEqual(1, callCount);

        // Activate called when getting instance
        var obj = objectPool.GetInstance();
        Assert.AreEqual(2, callCount);

        // Deactivate called when returning instance to pool
        objectPool.ReturnToPool(obj);
        Assert.AreEqual(3, callCount);
    }
}
