﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using UniRx;
using System;
using System.Collections;
using Object = UnityEngine.Object;

public class PlayerModelViewSpy : IPlayerModelView
{
    private ReactiveProperty<float> _healthReactive = new ReactiveProperty<float>(1f);
    public ReactiveProperty<float> HealthReactive { get { return _healthReactive; } }
	public float MoveSpeed { get; set; }
    public float AttackDuration { get; set; }
    public float AttackRange { get; set; }
    public float AttackDamage { get; set; }
    public Color HitParticlesColor { get; set; }
    public Vector3 HitParticlesPositionOffset { get; set; }
    public Vector3 Position { get; set; }
	public Vector3 Velocity { get; set; }
	public bool FlipX { get; set; }
	public bool IsMoving { get; set; }
    public bool IsAttacking { get; set; }
    public bool IsDead { get; set; }
    public int AttackCallCount { get; set; }
    public int PlayerHitAnimationCallCount { get; set; }
    public int DieCallCount { get; set; }

	public void Attack()
	{
		AttackCallCount++;
	}

    public void PlayHitAnimation()
    {
        PlayerHitAnimationCallCount++;
    }

    public void Die()
    {
        DieCallCount++;
    }
}

public class CameraShakeModelViewSpy : ICameraShakeModelView
{
    public int PlayShakeAnimationCallCount { get; set; }

    public void PlayShakeAnimation()
    {
        PlayShakeAnimationCallCount++;
    }
}

public class PlayerControllerTests : TestBehaviour
{
    private PlayerModelViewSpy _playerModelView;
    #pragma warning disable 0414
    private PlayerController _playerController;
    #pragma warning restore 0414

    [SetUp]
    public void SetUp()
    {
        _playerModelView = new PlayerModelViewSpy();
        _playerController = new PlayerController(_playerModelView);
    }

    [TearDown]
    public void TearDown()
    {
        _playerController.Dispose();
    }

    [Test]
    public void PlayerMovesOnInputEvent()
    {
        _playerModelView.Velocity = Vector3.zero;
        _playerModelView.FlipX = true;
        _playerModelView.MoveSpeed = 3f;

        EventStream.Publish(new PlayerMoveInputEvent { movement = 1f });

        Assert.AreNotEqual(Vector3.zero, _playerModelView.Velocity);
        Assert.False(_playerModelView.FlipX);
        Assert.True(_playerModelView.IsMoving);
    }

    [UnityTest]
    public IEnumerator PlayerAttackRateIsLimited()
    {
        _playerModelView.AttackCallCount = 0;
        _playerModelView.AttackDuration = 0.1f;

        EventStream.Publish(new PlayerAttackInputEvent());
        Assert.AreEqual(1, _playerModelView.AttackCallCount);

        // Immediately publishing another attack event shouldn't invoke our attack method
        EventStream.Publish(new PlayerAttackInputEvent());
        Assert.AreEqual(1, _playerModelView.AttackCallCount);

        // Wait long enough for AttackDuration to be over
        var waitDuration = _playerModelView.AttackDuration + 0.05f;
        var endTime = DateTime.Now.AddSeconds(waitDuration);

        while (DateTime.Now < endTime)
            yield return null;

        // Now publishing the attack event should invoke our attack method again
        EventStream.Publish(new PlayerAttackInputEvent());
        Assert.AreEqual(2, _playerModelView.AttackCallCount);
    }

    [TestCase(1f, 1f)]
    [TestCase(1f, 0.5f)]
    [TestCase(0f, 0.5f)]
    [TestCase(-1f, 0.5f)]
    [TestCase(0f, -0.5f)]
    public void PlayerLosesHealthWhenEnemyAttacks(float initialHealth, float damage)
    {
        _playerModelView.HealthReactive.Value = initialHealth;

        EventStream.Publish(new EnemyAttackEvent(damage));

        Assert.AreEqual(1, _playerModelView.PlayerHitAnimationCallCount);
        Assert.AreEqual(Mathf.Clamp01(initialHealth - damage), _playerModelView.HealthReactive.Value);
    }

    [Test]
    public void CameraShakesWhenPlayerIsHit()
    {
        var cameraShake = new CameraShakeModelViewSpy();
        new PlayerHitCameraShakeController(cameraShake);

        EventStream.Publish(new EnemyAttackEvent(0.3f));

        Assert.AreEqual(1, cameraShake.PlayShakeAnimationCallCount);
    }

    [TestCase(0, 0f)]
    [TestCase(0, 0.2f)]
    [TestCase(0, -1f)]
    [TestCase(1, 2f)]
    [TestCase(1, 1f)]
    public void PlayerDiesWhenHealthReachesZero(int expectedCallCount, float damage)
    {
        Assert.AreEqual(0, _playerModelView.DieCallCount);
        EventStream.Publish(new EnemyAttackEvent(damage));
        Assert.AreEqual(expectedCallCount, _playerModelView.DieCallCount);
    }

    [Test]
    public void HitParticlesAreSpawnedWhenPlayerIsHit()
    {
        var particleSystemName = "Hit Particles";

        Func<ParticleSystem> createInstanceDelegate = () => new GameObject(particleSystemName).AddComponent<ParticleSystem>();
        Action<ParticleSystem, bool> activateInstanceDelegate = (instance, active) =>
        {
            if (instance != null)
                instance.gameObject.SetActive(active);
        };

        var particlePool = new ObjectPool<ParticleSystem>(createInstanceDelegate, activateInstanceDelegate, 3);
        new HitParticlesController(particlePool);

        Assert.Null(GameObject.Find(particleSystemName));

        EventStream.Publish(new EnemyAttackEvent(0.3f));

        var particleSystem = GameObject.Find(particleSystemName);
        Assert.NotNull(particleSystem);
        Assert.True(particleSystem.activeInHierarchy);
    }
}
