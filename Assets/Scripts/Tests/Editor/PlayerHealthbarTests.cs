﻿using UnityEngine;
using NUnit.Framework;
using System;

public class HealthbarModelViewSpy : IHealthbarModelView
{
    public int SetHealthCallCount { get; set; }
    public float NewHealthValue { get; set; }

    public void SetHealth(float health)
    {
        SetHealthCallCount++;
        NewHealthValue = health;
    }
}

public class PlayerHealthbarTests : TestBehaviour
{
    [TestCase(0f, 1f)]
    [TestCase(0f, 2f)]
    [TestCase(1f, 0.5f)]
    [TestCase(1f, 0f)]
    [TestCase(1f, -1f)]
    public void HealthbarIsUpdatedWhenPlayerHealthChanges(float initialHealth, float newHealth)
    {
        var playerModelView = new PlayerModelViewSpy();
        playerModelView.HealthReactive.Value = initialHealth;

        var healthbarModelView = new HealthbarModelViewSpy();
        new PlayerHealthbarController(healthbarModelView, playerModelView);

        healthbarModelView.SetHealthCallCount = 0;
        healthbarModelView.NewHealthValue = -1f;

        playerModelView.HealthReactive.Value = newHealth;

        Assert.AreEqual(1, healthbarModelView.SetHealthCallCount);
        Assert.AreEqual(playerModelView.HealthReactive.Value, healthbarModelView.NewHealthValue);
    }
}
