﻿using UnityEngine;
using NUnit.Framework;
using UniRx;

public class LevelFinishTriggerModelViewStub : ILevelFinishTriggerModelView
{
    public IObservable<Unit> LevelCompleteStream { get; set; }
}

public class GeneralTests : TestBehaviour
{
    [Test]
    public void NextLevelIsLoadedWhenCurrentLevelIsCompleted()
    {
        var levelSwitchService = new LevelSwitchServiceSpy();
        var levelFinishTrigger = new LevelFinishTriggerModelViewStub();
        var levelCompleteStream = new Subject<Unit>();
        levelFinishTrigger.LevelCompleteStream = levelCompleteStream;

        new LevelFinishTriggerController(levelFinishTrigger, levelSwitchService);

        Assert.AreEqual(0, levelSwitchService.LoadLevelCallCount);
        levelCompleteStream.OnNext(Unit.Default);
        Assert.AreEqual(1, levelSwitchService.LoadLevelCallCount);
    }
}
