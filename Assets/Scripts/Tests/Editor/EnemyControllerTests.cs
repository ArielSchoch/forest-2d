﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using UniRx;
using System;
using System.Collections;
using Object = UnityEngine.Object;

public class EnemyModelViewSpy : IEnemyModelView
{
	private Subject<Unit> _onPlayAttackAnimationHitFrame = new Subject<Unit>();
    public IObservable<Unit> OnPlayAttackAnimationHitFrame { get { return _onPlayAttackAnimationHitFrame; } }

	private ReactiveProperty<Vector3> _enemyPositionReative = new ReactiveProperty<Vector3>();
    public ReactiveProperty<Vector3> EnemyPositionReactive { get { return _enemyPositionReative; } }

	private ReactiveProperty<float> _healthReactive = new ReactiveProperty<float>(1f);
    public ReactiveProperty<float> HealthReactive { get { return _healthReactive; } }

    public float MoveSpeed { get; set; }
    public float FollowDistance { get; set; }
    public float AttackDistance { get; set; }
    public float AttackDelay { get; set; }
    public float AttackDuration { get; set; }
	public float AttackDamage { get; set; }
	public Color HitParticlesColor { get; set; }
    public Vector3 HitParticlesPositionOffset { get; set; }
    public bool IsAttacking { get; set; }
    public bool IsDead { get; set; }
	public Vector3 Position { get; set; }
    public Vector3 Velocity { get; set; }
    public bool FlipX { get; set; }
    public bool IsMoving { get; set; }

	public int AttackCallCount { get; set; }
    public void Attack(Action onComplete)
    {
        AttackCallCount++;
    }

	public int PlayHitAnimationCallCount { get; set; }
    public void PlayHitAnimation()
    {
        PlayHitAnimationCallCount++;
    }

	public int DieCallCount { get; set; }
    public void Die()
    {
        DieCallCount++;
    }
}

[TestFixture]
public class EnemyControllerTests : TestBehaviour
{
    private EnemyModelViewSpy _modelView;
	private PlayerModelViewSpy _playerModelView;
    #pragma warning disable 0414
    private EnemyController _controller;
    #pragma warning restore 0414
	private GameObject _player;

    [SetUp]
    public void SetUp()
    {
		_player = new GameObject("Player");
        _modelView = new EnemyModelViewSpy();
		_playerModelView = new PlayerModelViewSpy();
        _controller = new EnemyController(_modelView, _playerModelView);
    }

	[TearDown]
	public void TearDown()
	{
		GameObject.DestroyImmediate(_player);
	}

    [TestCase(0f, 1f, 1)]
	[TestCase(0f, 2f, 0)]
    public void EnemyOnlyDiesWhenPlayerAttacksWhileInRange(float enemyPosX, float playerPosX, int shouldDie)
    {
		_playerModelView.Position = new Vector3(playerPosX, 0f, 0f);
		_modelView.EnemyPositionReactive.Value = new Vector3(enemyPosX, 0f, 0f);
		_modelView.HealthReactive.Value = 1f;
		_modelView.DieCallCount = 0;

        EventStream.Publish(new PlayerAttackEvent(1.5f, 1f));

        Assert.AreEqual(shouldDie, _modelView.DieCallCount);
    }

	[TestCase(0, 0.5f, 0)]
	[TestCase(1, 0.5f, 0)]
	[TestCase(2, 0.5f, 1)]
	[TestCase(3, 0.5f, 1)]
	[TestCase(4, 0.3f, 1)]
    public void EnemyOnlyDiesWhenPlayerAttacksEnoughTimes(int attackCount, float damage, int shouldDie)
    {
		_playerModelView.Position = new Vector3(1f, 0f, 0f);
		_modelView.EnemyPositionReactive.Value = new Vector3(0f, 0f, 0f);
		_modelView.HealthReactive.Value = 1f;
		_modelView.DieCallCount = 0;

		for (int i = 0; i < attackCount; i++)
        	EventStream.Publish(new PlayerAttackEvent(1.5f, damage));

        Assert.AreEqual(shouldDie, _modelView.DieCallCount);
    }

	[UnityTest]
    public IEnumerator EnemyAttacksPlayerWhenInRange()
    {
		_playerModelView.Position = new Vector3(1f, 0f, 0f);
		_modelView.EnemyPositionReactive.Value = new Vector3(0f, 0f, 0f);
		_modelView.HealthReactive.Value = 1f;
		_modelView.AttackDistance = 1.5f;
		_modelView.AttackDelay = 0.1f;
		_modelView.AttackDuration = 0f;
		_modelView.AttackCallCount = 0;

		// Wait for attack delay
        var waitDuration = _modelView.AttackDelay + 0.06f;
        var endTime = DateTime.Now.AddSeconds(waitDuration);

        while (DateTime.Now < endTime)
            yield return null;

        Assert.AreEqual(1, _modelView.AttackCallCount);
    }

	[UnityTest]
    public IEnumerator EnemyDoesntAttackPlayerWhenOutOfRange()
    {
		_playerModelView.Position = new Vector3(1f, 0f, 0f);
		_modelView.EnemyPositionReactive.Value = new Vector3(-1f, 0f, 0f);
		_modelView.HealthReactive.Value = 1f;
		_modelView.AttackDistance = 1.5f;
		_modelView.AttackDelay = 0.1f;
		_modelView.AttackDuration = 0f;
		_modelView.AttackCallCount = 0;

		// Wait for attack delay
        var waitDuration = _modelView.AttackDelay + 0.06f;
        var endTime = DateTime.Now.AddSeconds(waitDuration);

        while (DateTime.Now < endTime)
            yield return null;

        Assert.AreEqual(0, _modelView.AttackCallCount);
    }

	[UnityTest]
    public IEnumerator EnemyMovesTowardsPlayerWhenInRange()
    {
		_playerModelView.Position = new Vector3(4f, 0f, 0f);
		_modelView.EnemyPositionReactive.Value = new Vector3(0f, 0f, 0f);
		_modelView.HealthReactive.Value = 1f;
		_modelView.IsDead = false;
		_modelView.IsAttacking = false;
		_modelView.FlipX = true;
		_modelView.IsMoving = false;
		_modelView.FollowDistance = 6f;
		_modelView.MoveSpeed = 1.5f;
		_modelView.Velocity = Vector3.zero;

		yield return null;
		yield return null;

        Assert.Greater(_modelView.Velocity.x, 0f);
		Assert.False(_modelView.FlipX);
		Assert.True(_modelView.IsMoving);
    }

	[UnityTest]
    public IEnumerator EnemyDoesntMoveTowardsPlayerWhenOutOfRange()
    {
		_playerModelView.Position = new Vector3(4f, 0f, 0f);
		_modelView.EnemyPositionReactive.Value = new Vector3(-4f, 0f, 0f);
		_modelView.HealthReactive.Value = 1f;
		_modelView.IsDead = false;
		_modelView.IsAttacking = false;
		_modelView.FlipX = true;
		_modelView.IsMoving = true;
		_modelView.FollowDistance = 6f;
		_modelView.MoveSpeed = 1.5f;
		_modelView.Velocity = Vector3.zero;

		yield return null;
		yield return null;

        Assert.AreEqual(0f, _modelView.Velocity.x);
		Assert.True(_modelView.FlipX);
		Assert.False(_modelView.IsMoving);
    }
}
