﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UniRx;

public class GameInitializer : MonoBehaviour, IEventSubscriber
{
    [SerializeField] private GeneralFactory _generalFactory;
    [SerializeField] private PlayerFactory _playerFactory;

    private Camera _playerCamera;
    private Transform _worldTransform;
    private Transform _canvasTransform;
    private CompositeDisposable _disposer;
    private ILevelSwitchService _levelSwitchService;

    public CompositeDisposable Disposer
    {
        get { return _disposer ?? (_disposer = new CompositeDisposable()); }
        set { _disposer = value; }
    }

    public IEventAggregator EventStream { get { return EventAggregatorProvider.EventAggregator; } }

    private void Start()
    {
        // Setup event handling
		EventAggregatorProvider.EventAggregator = new EcsEventAggregator();
        // Initialize level switching service
        _levelSwitchService = new LevelSwitchService();
        // Start loading game scene
        LoadGame();
    }

    private void LoadGame()
    {
        _levelSwitchService.LoadGameScene(_ => Initialize());
    }

    private void Initialize()
    {
        FindSceneObjects();

        new GameManager(_levelSwitchService);
        var playerModelView = _playerFactory.InitializePlayer(_worldTransform, _canvasTransform, _playerCamera);

        _generalFactory.InitializeEnemies(playerModelView);
        _generalFactory.CreateHitParticlesController(_worldTransform);
        _generalFactory.CreateGameOverTextController();
        _generalFactory.CreateLevelFinishTriggerController(_levelSwitchService);

        EventStream.GetEvent<UnloadSceneEvent>().Subscribe(_ => Dispose()).AddTo(Disposer);
        EventStream.Publish(new SceneLoadedEvent());
    }

    private void FindSceneObjects()
    {
        // Find InputBroker
        InputBrokerProvider.InputBroker = GameObject.FindObjectOfType<InputBroker>();
        // Find player camera
        _playerCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        // Find world transform
        _worldTransform = GameObject.Find("World").transform;
        // Find main canvas
        _canvasTransform = GameObject.Find("Canvas").transform;
    }

    private void Dispose()
    {
        Disposer.Dispose();
    }
}
