﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

// Editor script that automatically loads the Init scene when pressing play
[InitializeOnLoad]
public class PlayModeStartSceneSetter
{
    private const string InitScenePath = "Assets/Scenes/Init.unity";

    static PlayModeStartSceneSetter()
    {
        SetPlayModeStartScene(InitScenePath);
    }

    private static void SetPlayModeStartScene(string scenePath)
    {
        SceneAsset startScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(scenePath);
        if (startScene != null)
            EditorSceneManager.playModeStartScene = startScene;
        else
            Debug.Log("Could not find scene " + scenePath);
    }
}
