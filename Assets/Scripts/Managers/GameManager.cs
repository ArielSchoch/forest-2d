﻿using UnityEngine;
using UniRx;

public class GameManager : Service
{
    private const KeyCode SpaceKeyCode = KeyCode.Space;

    public GameManager(ILevelSwitchService levelSwitchService)
    {
        EventStream.GetEvent<PlayerDiedEvent>().Subscribe(_ =>
        {
            // Load first level again when SPACE is pressed after player has died
            InputBroker.OnKeyDown(SpaceKeyCode).Subscribe(__ => levelSwitchService.LoadLevel(0)).AddTo(Disposer);
        }).AddTo(Disposer);
    }
}
