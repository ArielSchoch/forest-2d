﻿using UnityEngine;
using UniRx;

public abstract class ModelView : MonoBehaviour
{
    public IInputBroker InputBroker { get { return InputBrokerProvider.InputBroker; } }

    private CompositeDisposable _disposer;
    public CompositeDisposable Disposer
    {
        get { return _disposer ?? (_disposer = new CompositeDisposable()); }
        set { _disposer = value; }
    }

    protected virtual void Awake()
    {
        Initialize();
    }

    protected virtual void Initialize() {}
}
