﻿
public abstract class Service : EventSubscriber
{
    public IInputBroker InputBroker { get { return InputBrokerProvider.InputBroker; } }
}
