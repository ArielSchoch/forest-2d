﻿using UniRx;

public abstract class EventSubscriber : IEventSubscriber
{
    public IEventAggregator EventStream { get { return EventAggregatorProvider.EventAggregator; } }

    private CompositeDisposable _disposer;
    public CompositeDisposable Disposer
    {
        get { return _disposer ?? (_disposer = new CompositeDisposable()); }
        set { _disposer = value; }
    }

    public EventSubscriber()
    {
        ListenForDisposeEvent();
    }

    public virtual void ListenForDisposeEvent()
    {
        EventStream.GetEvent<UnloadSceneEvent>().Subscribe(_ => Dispose()).AddTo(Disposer);
    }

    public virtual void Dispose()
    {
        Disposer.Dispose();
    }
}
