using UnityEngine;
using UniRx;
using System.Threading;

public abstract class Controller : EventSubscriber
{
    public IInputBroker InputBroker { get { return InputBrokerProvider.InputBroker; } }
}

public abstract class Controller<T> : Controller where T : class
{
    protected T _modelView;

    public Controller(T modelView)
    {
        _modelView = modelView;
    }
}
