﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UniRx;

public class PlayerController : Controller<IPlayerModelView>
{
    private readonly PlayerAttackEvent _playerAttackEvent;
    private readonly SpawnHitParticlesEvent _spawnHitParticlesEvent = new SpawnHitParticlesEvent();
	private readonly CameraShakeEvent _cameraShakeEvent = new CameraShakeEvent();

    private bool _CanAttack { get { return !_modelView.IsDead && !_modelView.IsAttacking; } }

	public PlayerController(IPlayerModelView modelView) : base(modelView)
    {
        _playerAttackEvent = new PlayerAttackEvent(modelView.AttackRange, modelView.AttackDamage);

        ListenForPlayerInput();
        TakeDamageWhenEnemyAttacks();
        DieWhenHealthEqualsZero();
    }

    private void ListenForPlayerInput()
    {
        EventStream.GetEvent<PlayerMoveInputEvent>().Subscribe(evt => Move(evt.movement)).AddTo(Disposer);
        EventStream.GetEvent<PlayerAttackInputEvent>().Subscribe(_ => Attack()).AddTo(Disposer);
    }

    private void TakeDamageWhenEnemyAttacks()
    {
        EventStream.GetEvent<EnemyAttackEvent>().Subscribe(evt => TakeDamage(evt.Damage)).AddTo(Disposer);
    }

    private void DieWhenHealthEqualsZero()
    {
        _modelView.HealthReactive.Where(health => health <= 0f).Subscribe(_ => Die()).AddTo(Disposer);
    }

    private void Move(float movement)
    {
        if (_modelView.IsDead) return;

        var velocity = _modelView.Velocity;
        velocity.x = movement * _modelView.MoveSpeed;
        _modelView.Velocity = velocity;

        UpdateSpriteDirection();
        UpdateIsMoving();
    }

    private void UpdateSpriteDirection()
    {
        if (_modelView.Velocity.x != 0f)
            _modelView.FlipX = _modelView.Velocity.x < 0f;
    }

    private void UpdateIsMoving()
    {
        _modelView.IsMoving = Mathf.Abs(_modelView.Velocity.x) > 0;
    }

    private void Attack()
    {
        if (!_CanAttack) return;

        _modelView.IsAttacking = true;
        _modelView.Attack();

        EventStream.Publish(_playerAttackEvent);

        Observable.Timer(TimeSpan.FromSeconds(_modelView.AttackDuration)).Subscribe(_ => _modelView.IsAttacking = false).AddTo(Disposer);
    }

    private void TakeDamage(float damage)
    {
        _modelView.HealthReactive.Value = Mathf.Clamp01(_modelView.HealthReactive.Value - damage);
        _modelView.PlayHitAnimation();

        SpawnHitParticles();
        EventStream.Publish(_cameraShakeEvent);
    }

    private void SpawnHitParticles()
    {
        _spawnHitParticlesEvent.Position = _modelView.Position + _modelView.HitParticlesPositionOffset;
        _spawnHitParticlesEvent.Color = _modelView.HitParticlesColor;
        EventStream.Publish(_spawnHitParticlesEvent);
    }

    private void Die()
    {
        _modelView.IsDead = true;
        _modelView.Die();
        EventStream.Publish(new PlayerDiedEvent());
    }
}
