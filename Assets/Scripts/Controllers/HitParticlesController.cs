﻿using UnityEngine;
using UniRx;
using System;

public class HitParticlesController : Controller
{
    private ObjectPool<ParticleSystem> _particleSystemPool;
    private const float HitParticleDuration = 1f;

    public HitParticlesController(ObjectPool<ParticleSystem> particleSystemPool)
    {
        _particleSystemPool = particleSystemPool;
        EventStream.GetEvent<SpawnHitParticlesEvent>().Subscribe(SpawnHitParticles).AddTo(Disposer);
    }

    public void SpawnHitParticles(SpawnHitParticlesEvent evt)
    {
        var instance = _particleSystemPool.GetInstance();
        instance.transform.position = evt.Position;

        var mainModule = instance.main;
        mainModule.startColor = evt.Color;

        instance.Play();

        Observable.Timer(TimeSpan.FromSeconds(HitParticleDuration))
            .Subscribe(_ => _particleSystemPool.ReturnToPool(instance))
            .AddTo(Disposer);
    }
}
