﻿using UnityEngine;
using UniRx;

public class LevelFinishTriggerController : Controller<ILevelFinishTriggerModelView>
{
    public LevelFinishTriggerController(ILevelFinishTriggerModelView modelView, ILevelSwitchService levelSwitchService) : base(modelView)
    {
        // Load next level when level is complete
        _modelView.LevelCompleteStream.Subscribe(_ => levelSwitchService.LoadLevel(levelSwitchService.CurrentLevel + 1)).AddTo(Disposer);
    }
}
