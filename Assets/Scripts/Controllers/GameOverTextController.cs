﻿using UnityEngine;
using UniRx;

public class GameOverTextController : Controller<GameOverTextModelView>
{
    public GameOverTextController(GameOverTextModelView modelView) : base(modelView)
    {
        _modelView.Visible = false;
        EventStream.GetEvent<PlayerDiedEvent>().Subscribe(_ => _modelView.Visible = true).AddTo(Disposer);
    }
}
