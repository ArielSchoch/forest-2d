﻿using UnityEngine;
using UniRx;

public class PlayerInputController : Controller<IPlayerInputModelView>
{
    private PlayerAttackInputEvent _playerAttackInputEvent = new PlayerAttackInputEvent();
    private PlayerMoveInputEvent _playerMoveInputEvent = new PlayerMoveInputEvent();

    public PlayerInputController(IPlayerInputModelView modelView) : base(modelView)
    {
        _modelView.AttackInputStream
            .Subscribe(_ => EventStream.Publish(_playerAttackInputEvent))
            .AddTo(Disposer);

        _modelView.MoveInputStream
            .Subscribe(OnMove)
            .AddTo(Disposer);
    }

    private void OnMove(float amount)
    {
        _playerMoveInputEvent.movement = amount;
        EventStream.Publish(_playerMoveInputEvent);
    }
}
