using UnityEngine;
using UniRx;
using System;

public class EnemyController : Controller<IEnemyModelView>
{
    private readonly IPlayerModelView _playerModelView;
    private readonly EnemyAttackEvent _enemyAttackEvent;
    private readonly SpawnHitParticlesEvent _spawnHitParticlesEvent = new SpawnHitParticlesEvent();
    private readonly ReactiveProperty<float> _playerDistance = new ReactiveProperty<float>();

    private bool _CanMove { get { return !_modelView.IsDead && !_modelView.IsAttacking; } }
    private bool _CanAttack { get { return !_playerModelView.IsDead && !_modelView.IsAttacking; } }

    public EnemyController(IEnemyModelView modelView, IPlayerModelView playerModelView) : base(modelView)
	{
        _playerModelView = playerModelView;
        _enemyAttackEvent = new EnemyAttackEvent(modelView.AttackDamage);

        Observable.EveryUpdate().Subscribe(_ => OnUpdate()).AddTo(Disposer);

        DieWhenHealthEqualsZero();
        TakeDamageWhenPlayerAttacks();
        AttackWhenPlayerIsInRange();
        PublishAttackEventWhenAnimationEnds();
    }

    private void DieWhenHealthEqualsZero()
    {
        _modelView.HealthReactive.Where(health => health <= 0f).Subscribe(_ => Die()).AddTo(Disposer);
    }

    private void TakeDamageWhenPlayerAttacks()
    {
        EventStream.GetEvent<PlayerAttackEvent>()
            .Where(evt => ShouldTakeDamageFromPlayer(evt.AttackRange))
            .Subscribe(evt => TakeDamage(evt.Damage))
            .AddTo(Disposer);
    }

    private void AttackWhenPlayerIsInRange()
    {
        _playerDistance.Skip(1)
            .Where(distance => IsInAttackRange(distance) && _CanAttack)
            .Subscribe(_ => Attack())
            .AddTo(Disposer);
    }

    private void PublishAttackEventWhenAnimationEnds()
    {
        _modelView.OnPlayAttackAnimationHitFrame
            .Where(__ => IsInAttackRange(_playerDistance.Value) && !_modelView.IsDead)
            .Subscribe(__ => EventStream.Publish(_enemyAttackEvent))
            .AddTo(Disposer);
    }

    private bool ShouldTakeDamageFromPlayer(float attackRange)
    {
        var distance = Vector2.Distance(_modelView.EnemyPositionReactive.Value, _playerModelView.Position);
        return distance <= attackRange && !_modelView.IsDead;
    }

    private void OnUpdate()
    {
        var playerPos = _playerModelView.Position;
        var enemyPos = _modelView.EnemyPositionReactive.Value;

        var distance = Vector2.Distance(playerPos, enemyPos);
        _playerDistance.Value = distance;

        if (IsInFollowRange(distance) && !_playerModelView.IsDead)
            Move(Mathf.Sign(playerPos.x - enemyPos.x));
        else
            _modelView.IsMoving = false;
    }

    private void Move(float direction)
    {
        if (!_CanMove) return;

        var velocity = _modelView.Velocity;
        velocity.x = direction * _modelView.MoveSpeed;
        _modelView.Velocity = velocity;

        UpdateSpriteDirection();
        UpdateIsMoving();
    }

    private void UpdateSpriteDirection()
    {
        if (_modelView.Velocity.x != 0f)
            _modelView.FlipX = _modelView.Velocity.x < 0f;
    }

    private void UpdateIsMoving()
    {
        _modelView.IsMoving = Mathf.Abs(_modelView.Velocity.x) > 0;
    }

    private void TakeDamage(float damage)
    {
        _modelView.HealthReactive.Value = Mathf.Clamp01(_modelView.HealthReactive.Value - damage);
        _modelView.PlayHitAnimation();

        SpawnHitParticles();
    }

    private void SpawnHitParticles()
    {
        _spawnHitParticlesEvent.Position = _modelView.Position + _modelView.HitParticlesPositionOffset;
        _spawnHitParticlesEvent.Color = _modelView.HitParticlesColor;
        EventStream.Publish(_spawnHitParticlesEvent);
    }

    private void Attack()
    {
        _modelView.IsAttacking = true;

        Observable.Timer(TimeSpan.FromSeconds(_modelView.AttackDelay)).Subscribe(_ =>
        {
            if (_modelView.IsDead)
                _modelView.IsAttacking = false;
            else
                _modelView.Attack(() => _modelView.IsAttacking = false);
        }).AddTo(Disposer);
    }

    private void Die()
    {
        _modelView.IsDead = true;
        _modelView.Die();
    }

    private bool IsInFollowRange(float distance)
    {
        return distance <= _modelView.FollowDistance;
    }

    private bool IsInAttackRange(float distance)
    {
        return distance <= _modelView.AttackDistance;
    }
}
