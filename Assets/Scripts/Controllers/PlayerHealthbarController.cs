﻿using UnityEngine;
using UniRx;

public class PlayerHealthbarController : Controller<IHealthbarModelView>
{
    private IPlayerModelView _playerModelView;

    public PlayerHealthbarController(IHealthbarModelView modelView, IPlayerModelView playerModelView) : base(modelView)
    {
        _playerModelView = playerModelView;
        _playerModelView.HealthReactive.Subscribe(health => _modelView.SetHealth(health)).AddTo(Disposer);
    }
}
