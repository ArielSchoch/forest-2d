﻿using UnityEngine;
using UniRx;

public class PlayerHitCameraShakeController : Controller<ICameraShakeModelView>
{
	public PlayerHitCameraShakeController(ICameraShakeModelView modelView) : base(modelView)
	{
		EventStream.GetEvent<CameraShakeEvent>().Subscribe(_ => _modelView.PlayShakeAnimation()).AddTo(Disposer);
	}
}
