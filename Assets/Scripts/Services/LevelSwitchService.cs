﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSwitchService : Service, ILevelSwitchService
{
    private const string InitSceneName = "Init";
    private const string LevelSceneName = "Level {0}";
    private const int MaxLevel = 2;

    public int CurrentLevel
    {
        get { return PlayerPrefs.GetInt("Level", 0); }
        private set { PlayerPrefs.SetInt("Level", value); }
    }

    public void LoadLevel(int level)
    {
        if (level > MaxLevel) return;

        CurrentLevel = level;
        LoadInitScene();
    }

    public void LoadGameScene(Action<AsyncOperation> onCompleted)
    {
        var sceneName = string.Format(LevelSceneName, CurrentLevel);
        SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive).completed += onCompleted;
    }

    private void LoadInitScene()
    {
        EventStream.Publish(new UnloadSceneEvent());
        SceneManager.LoadScene(InitSceneName);
    }
}
